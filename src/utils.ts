import { SystemChannelFlags } from "discord.js"


export enum linuxColors {
    reset = "\x1b[0m",
    red = "\x1b[31m",
    yellow = "\x1b[33m"
}


export enum warningLevel {
    log,
    warning,
    error,
}
export let invite = "> "

export function clearLine() {
    process.stdout.write("\r\x1b[K")
}
export function showInvite() {
    clearLine()
    process.stdout.write(invite)
}



export function logger(message: string, componentName: string, level: warningLevel = warningLevel.log) {
    let level_color = ""
    switch (level) {
        case warningLevel.log:
            level_color = linuxColors.reset
            break
        case warningLevel.warning:
            level_color = linuxColors.yellow
            break
        case warningLevel.error:
            level_color = linuxColors.red
            break
    }

    let date = new Date
    date.toLocaleDateString()
    clearLine()
    process.stdout.write(`(${date.toLocaleDateString()}) ${date.toLocaleTimeString()} ${level_color} [${componentName}] : ${message} \n ${linuxColors.reset}`)
    showInvite()

}