import * as Discord from "discord.js"
import { readFileSync } from "fs"
import * as readline from "readline"
import { logger, warningLevel, showInvite } from "./utils"
import { lookup } from "dns"

console.log("starting...")

const client = new Discord.Client()
export let prefix = "$"
let token = readFileSync("token").toString().trim()

function commandInterpreter(input: string) {
    let data = input.trim()
    if (data.length > 0) {
        let args = input.split(" ")
        switch (args[0]) {
            case "exit":
                process.exit(0)
            case "echo":
                logger(args.slice(1).join(" "), "cli")
        }
    }
    showInvite()



}

let rl = readline.createInterface(process.stdin, process.stdout)

client.on("ready", () => {
    logger(`Logged in as ${client.user?.tag}!`,"bot")
    logger("this is a warning","main", warningLevel.warning)
    logger("this is an error","main", warningLevel.error)

    rl.addListener("line", commandInterpreter)
})

client.login(token)